class AddTownToArea < ActiveRecord::Migration
  def change
    add_column :areas, :town, :string
  end
end
