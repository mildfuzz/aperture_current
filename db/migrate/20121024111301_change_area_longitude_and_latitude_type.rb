class ChangeAreaLongitudeAndLatitudeType < ActiveRecord::Migration
   def up
  	change_column :areas, :latitude, :float
  	change_column :areas, :longitude, :float
  end

  def down
  	change_column :areas, :latitude, :string
  	change_column :areas, :longitude, :string
  end
end
