class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :name
      t.text :description
      t.string :longitude
      t.string :latitude
      t.string :postcode

      t.timestamps
    end
  end
end
