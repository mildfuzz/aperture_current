class AddSubtitleToLocality < ActiveRecord::Migration
  def change
    add_column :localities, :subtitle, :string
  end
end
