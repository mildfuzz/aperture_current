class AddStreetToArea < ActiveRecord::Migration
  def change
    add_column :areas, :street, :string
  end
end
