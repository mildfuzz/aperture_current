class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.text :description
      t.integer :area_id
      t.string :longitude
      t.string :latitude
      t.string :postcode

      t.timestamps
    end
  end
end
