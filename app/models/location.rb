class Location < ActiveRecord::Base
  belongs_to :locality
  belongs_to :area
  has_many :images
  attr_accessible :area_id, :description, :latitude, :longitude, :name, :postcode, :locality_id

  accepts_nested_attributes_for :images

  def location
  	[:latitude, :longitude]
  end

  acts_as_gmappable
  def gmaps4rails_address
    "#{self.street}, #{self.town}, #{self.country}" 
  end

  def gmaps4rails_title
    self.name
  end

  def gmaps4rails_infowindow
    "<b>#{self.name}</b><br /><i>#{self.description}</i><br /><br />#{self.gmaps4rails_address}<br /><i>#{self.location.join(', ')}</i>"
  end
end
