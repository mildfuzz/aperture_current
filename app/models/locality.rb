class Locality < ActiveRecord::Base
  has_many :locations
  attr_accessible :description, :name, :subtitle

end
