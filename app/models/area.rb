class Area < ActiveRecord::Base
  has_many :locations
  attr_accessible :description, :latitude, :longitude, :name, :postcode

  def address
  	"#{self.street}, #{self.town}, #{self.postcode}"
  end
end
