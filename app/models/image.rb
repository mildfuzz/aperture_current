class Image < ActiveRecord::Base
  belongs_to :location
  attr_accessible :description, :location_id, :name, :avatar
  has_attached_file :avatar, :styles => {:thumb=> "118x118#", :half => "600x350#", :full=>"1380x776>"}

end
