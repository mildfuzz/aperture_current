class LocalitiesController < ActionController::Base
  protect_from_forgery
  def index
  	@locales = Locality.find(:all)
  	raise @locales.to_yaml
  end
end
