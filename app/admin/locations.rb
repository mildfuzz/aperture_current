ActiveAdmin.register Location do
  form multipart: true do |f|
     f.inputs do
 	   f.input :locality
 	   f.input :area
     f.input :location do |o|
           gmaps("markers" => {data: o.to_gmaps4rails}, "map_options" =>  { auto_zoom: false, zoom: 15 })
     end
  
 	   f.input :name
 	   f.input :description
 	   f.input :postcode
     
      f.has_many :images do |i| 
        i.input :image, 
                :as => :file, 
                :multipart => true, 
                :label => "Image" 
                
      end 
    
 end

    f.buttons
  end
end
