ActiveAdmin.register Image do
  filter :location
  index :as => :grid do |image|
    link_to(image_tag(image.avatar.url(:thumb)), admin_image_path(image))
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
      f.inputs "Image", :multipart => true do
         f.input :location
        f.input :name
        f.input :avatar, :hint => "current image: #{f.template.image_tag(f.object.avatar.url(:thumb)) if f.object.avatar.file? }" 
        f.input :description
       
        
        
      end
   	  f.buttons
 	end
  show do |image|
  	  image_tag(image.avatar.url(:full))
      div do
        # simple_format image.body
      end


    end
  show do |image|
      attributes_table do
      	row :location do 
      		content_tag :h3, image.location.name
      	end
        row :name
        row :description
        row :avatar do
          image_tag(image.avatar.url(:half))
        end
      end
      active_admin_comments
    end
end	