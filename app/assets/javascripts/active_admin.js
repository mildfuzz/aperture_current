//= require active_admin/base
//= require_tree ./plugins/.
//= require gmaps4rails/gmaps4rails.base
//= require gmaps4rails/gmaps4rails.googlemaps

$(document).ready(function(){
	$('input[type=file]').validateImageUpload();
});
