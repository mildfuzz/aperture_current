(function($){
  

  $.fn.validateImageUpload = function(size){
    //Accepts form file input fields $('input[type=file]').validateImageUpload();
    //Also accepts size validation (in K), default is 700. Set to false in order to skip check
    if(typeof size != "number" && size%1 != 0){size = 700}
    var validate = function(x){
      var files = $(x).get(0).files, that = x;
      for (i = 0; i < files.length; i++){

        if(!files[i].type.match(/(gif|jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG)/)){
          alert('Must be an image file.');
          $(that).val('');
          return false;
        }

        if(size && files[i].size > size*1000){
          alert('File be an under '+size+'k.');
          $(that).val('');
          return false;
        }

        return true;
    }
  }

  

    $('body').on('change',$(this).selector,function(){
        if(validate(this)){
              return true;
            } else {
              $(this).val("");
              return false;
            }
        });

}
})(jQuery)
